<?php
//Template Name: About

add_action( 'genesis_entry_content', 'wst_display_team' );
function wst_display_team() {
	$context         = Timber::get_context();
	$templates       = array( 'about.twig' );
	$args            = array(
		'post_type'      => array( 'team' ),
		'nopaging'       => true,
		'posts_per_page' => '-1',
	);
	$context['team'] = get_posts( $args );

	Timber::render( $templates, $context );
}

genesis();
