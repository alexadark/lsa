<?php
//Template Name: projects

add_action( 'genesis_entry_content', 'wst_display_projects', 15 );
function wst_display_projects() {
	$context   = Timber::get_context();
	$templates = array( 'projects.twig' );
	Timber::render( $templates, $context );
}



genesis();
