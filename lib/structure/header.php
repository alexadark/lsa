<?php

// Displays custom logo.
add_action( 'genesis_site_title', 'the_custom_logo', 0 );
add_action( 'genesis_after_header', 'wst_display_hero' );
function wst_display_hero() {
	if ( is_page_template( 'contact.php' ) || is_singular( array( 'post', 'team' ) ) ) {
		return;
	}
	$context   = Timber::get_context();
	$templates = array( 'hero.twig' );
	Timber::render( $templates, $context );
}
