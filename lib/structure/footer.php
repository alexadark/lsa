<?php
//* Customize the entire footer
remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'wst_custom_footer' );
function wst_custom_footer() {
	$context               = Timber::get_context();
	$templates             = array( 'footer.twig' );
	$context['footerMenu'] = Timber::get_widgets( 'footer-menu' );
	$context['date']       = date( 'Y' );
	Timber::render( $templates, $context );

}
