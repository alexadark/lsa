<?php

add_action( 'widgets_init', 'wst_register_widget_areas' );
function wst_register_widget_areas() {

	$widgets_areas = array(

		array(
			'name'        => __( 'Footer Menu', CHILD_TEXT_DOMAIN ),
			'id'          => 'footer-menu',
			'description' => __( 'This is the widget area for the footer menu', CHILD_TEXT_DOMAIN ),
		),

	);

	foreach ( $widgets_areas as $widget_area ) {

		genesis_register_sidebar( $widget_area );

	}
}
