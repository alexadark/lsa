<?php

add_action( 'wp_enqueue_scripts', 'wst_enqueue_scripts_styles' );
/**
 * Enqueues scripts and styles.
 *
 * @since 1.0.0
 */
function wst_enqueue_scripts_styles() {
	 wp_dequeue_style( CHILD_TEXT_DOMAIN );

	wp_enqueue_style(
		CHILD_TEXT_DOMAIN . '-fonts',
		'//fonts.googleapis.com/css?family=Muli:400,600',
		array(),
		CHILD_THEME_VERSION
	);
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_style( 'swiper-styles', CHILD_CSS . 'swiper.min.css', array(), CHILD_THEME_VERSION );

	wp_enqueue_style( CHILD_TEXT_DOMAIN . '-styles', CHILD_CSS . 'style.css' );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script(
		CHILD_TEXT_DOMAIN . '-responsive-menu',
		CHILD_JS . "responsive-menus{$suffix}.js",
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);
	wp_localize_script(
		CHILD_TEXT_DOMAIN . '-responsive-menu',
		'genesis_responsive_menu',
		wst_responsive_menu_settings()
	);
	wp_enqueue_script(
		'js-match-height',
		CHILD_JS . 'jquery.matchHeight.min.js',
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);

	wp_enqueue_script(
		'theme-js',
		CHILD_JS . 'theme.js',
		array( 'jquery', 'js-match-height' ),
		CHILD_THEME_VERSION,
		true
	);
	wp_enqueue_script(
		'swiper-js',
		CHILD_JS . 'swiper.min.js',
		array( 'jquery' ),
		'4.4.2',
		true
	);
	wp_enqueue_script(
		'swiper-init-js',
		CHILD_JS . 'swiper-init.js',
		array( 'jquery', 'swiper-js' ),
		CHILD_THEME_VERSION,
		true
	);

}
