<?php

/**
 * Setup your child theme
 *
 * @package     LSA
 * @since       1.0.0
 * @author      Alexandra Spalato
 * @license     GNU General Public License 2.0+
 */

// Sets up the Theme.
require_once 'theme-defaults.php';

/*-----------------------------------------------------------
	LOCALIZATION
/*------------------------------------------------------------*/


add_action( 'after_setup_theme', 'wst_localization_setup' );
/**
 * Sets localization (do not remove).
 *
 * @since 1.0.0
 */
function wst_localization_setup() {
	load_child_theme_textdomain( CHILD_TEXT_DOMAIN, get_stylesheet_directory() . '/languages' );

}

/*-----------------------------------------------------------
	RESPONSIVE MENU SETTINGS
/*------------------------------------------------------------*/

/**
 * Defines responsive menu settings.
 *
 * @since 2.3.0
 */
function wst_responsive_menu_settings() {
	$settings = array(
		'mainMenu'         => __( 'Menu', CHILD_TEXT_DOMAIN ),
		'menuIconClass'    => 'dashicons-before dashicons-menu',
		'subMenu'          => __( 'Submenu', CHILD_TEXT_DOMAIN ),
		'subMenuIconClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'      => array(
			'combine' => array(
				'.nav-primary',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

/*-----------------------------------------------------------
	CONTENT WIDTH
/*------------------------------------------------------------*/

// Sets the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 702; // Pixels.
}

/*-----------------------------------------------------------
	THEME SUPPORTS
/*------------------------------------------------------------*/

// Adds support for HTML5 markup structure.
add_theme_support(
	'html5',
	array(
		'caption',
		'comment-form',
		'comment-list',
		'gallery',
		'search-form',
	)
);

// Adds support for accessibility.
add_theme_support(
	'genesis-accessibility',
	array(
		'404-page',
		'drop-down-menu',
		'headings',
		'rems',
		'search-form',
		'skip-links',
	)
);

// Adds viewport meta tag for mobile browsers.
add_theme_support(
	'genesis-responsive-viewport'
);

// Adds custom logo in Customizer > Site Identity.
add_theme_support(
	'custom-logo',
	array(
		'height'      => 120,
		'width'       => 700,
		'flex-height' => true,
		'flex-width'  => true,
	)
);

// Renames primary and secondary navigation menus.
add_theme_support(
	'genesis-menus',
	array(
		'primary'   => __( 'Header Menu', 'genesis-sample' ),
		'secondary' => __( 'Footer Menu', 'genesis-sample' ),
	)
);

// Adds support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Adds support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 3 );

/*-----------------------------------------------------------
	REMOVE THINGS
/*------------------------------------------------------------*/

// Removes site layouts.
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );



add_action( 'genesis_theme_settings_metaboxes', 'wst_remove_metaboxes' );
/**
 * Removes output of unused admin settings metaboxes.
 *
 * @since 2.6.0
 *
 * @param string $_genesis_admin_settings The admin screen to remove meta boxes from.
 */
function wst_remove_metaboxes( $_genesis_admin_settings ) {

	remove_meta_box( 'genesis-theme-settings-header', $_genesis_admin_settings, 'main' );
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_admin_settings, 'main' );

}

add_filter( 'genesis_customizer_theme_settings_config', 'genesis_sample_remove_customizer_settings' );
/**
 * Removes output of header settings in the Customizer.
 *
 * @since 2.6.0
 *
 * @param array $config Original Customizer items.
 *
 * @return array Filtered Customizer items.
 */
function genesis_sample_remove_customizer_settings( $config ) {

	unset( $config['genesis']['sections']['genesis_header'] );

	return $config;

}

/*-----------------------------------------------------------
	GRAVATAR
/*------------------------------------------------------------*/

add_filter( 'genesis_author_box_gravatar_size', 'wst_author_box_gravatar' );
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 2.2.3
 *
 * @param int $size Original icon size.
 *
 * @return int Modified icon size.
 */
function wst_author_box_gravatar( $size ) {

	return 90;

}

add_filter( 'genesis_comment_list_args', 'wst_comments_gravatar' );
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 2.2.3
 *
 * @param array $args Gravatar settings.
 *
 * @return array Gravatar settings with modified size.
 */
function wst_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}
/*-----------------------------------------------------------
	IMAGE SIZES
/*------------------------------------------------------------*/
add_image_size('archive', 616, 460, true);
/*-----------------------------------------------------------
	TIMBER
/*------------------------------------------------------------*/

// If the Timber plugin isn't activated, print a notice in the admin.
if ( ! class_exists( 'Timber' ) ) {
	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);
	return;
}

add_filter( 'timber_context', 'wst_add_to_context' );
/**
 * add post as timberPost
 *
 * @since 1.0.0
 *
 * @param $context
 *
 * @return mixed
 */
function wst_add_to_context( $context ) {
	$post            = new TimberPost();
	$context['post'] = $post;
	return $context;
}

/*-----------------------------------------------------------
	ACF
/*------------------------------------------------------------*/
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page();

}

