<?php

/**
 * LSA theme
 *
 *
 * @since 1.0.0
 * @author Alexandra Spalato <alexaspalato@gmail.com>
 * @licence GNU General Public License 2.0+
 */


// Starts the engine.
require_once get_template_directory() . '/lib/init.php';

require_once 'lib/init.php';

require_once 'lib/functions/autoload.php';
