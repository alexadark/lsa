<?php
//Template Name: projects landing

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action( 'genesis_before_footer', 'wst_displays_projects_landing', 1 );
function wst_displays_projects_landing() {
	$context   = Timber::get_context();
	$templates = array( 'projectsLanding.twig' );
	Timber::render( $templates, $context );
}



genesis();
