<?php
//* Remove the entry meta in the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action( 'genesis_entry_header', 'wst_display_member_sidebar' );
function wst_display_member_sidebar() {
	$context   = Timber::get_context();
	$templates = array( 'memberSidebar.twig' );
	Timber::render( $templates, $context );
}
add_action( 'genesis_entry_content', 'wst_display_member_content' );
function wst_display_member_content() {
	$context   = Timber::get_context();
	$templates = array( 'memberContent.twig' );
	Timber::render( $templates, $context );
}

genesis();
