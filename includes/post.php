<div class="postCard">
<div class="postImage"><?php genesis_image( array( 'size' => 'archive' ) ); ?></div>
<a href="<?php the_permalink(); ?>">
<div class="postContent">

	<div class="postDate"><?php the_date(); ?></div>
	<h3><?php the_title(); ?></h3>
	<?php the_excerpt(); ?>
	<div class="arrow"><img src="<?php echo CHILD_IMG; ?>arrow.png" alt=""></div>
</div>
</a>
</div>
