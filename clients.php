<?php
//Template Name: clients

add_action( 'genesis_entry_content', 'wst_display_clients' );
function wst_display_clients() {
	$context   = Timber::get_context();
	$templates = array( 'clients.twig' );
	Timber::render( $templates, $context );
}



genesis();
