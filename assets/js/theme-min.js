/**
 * LSA entry point.
 *
 * @package GenesisSample\JS
 * @author  StudioPress
 * @license GPL-2.0+
 */
var genesisSample=function(t){"use strict";var i=function(){var e=0;"fixed"===t(".site-header").css("position")&&(e=t(".site-header").outerHeight()),t(".hero").css("margin-top",e),t(".single .site-inner").css("margin-top",e)},e;return{init:function(){i(),t(window).resize(function(){i()}),void 0!==wp.customize&&wp.customize.bind("change",function(e){setTimeout(function(){i()},1500)}),t(".enews input").wrapAll("<div></div>"),t(".area").matchHeight(),t(".teamMember-body").matchHeight(),t(".project-body .project-content").matchHeight(),t(".postCard .postContent").matchHeight()}}}(jQuery);jQuery(window).on("load",genesisSample.init);