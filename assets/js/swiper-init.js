jQuery(document).ready(function ($) {
    var clientSlider = new Swiper('.swiperClients', {
        spaceBetween: 30,
        slidesPerView: 5,
        loop: true,
        autoplay: {
            delay: 5000,
        },
        // setWrapperSize: true,
        // navigation: {
        //     nextEl: '.swiper-button-next',
        //     prevEl: '.swiper-button-prev',
        // },
        breakpoints: {
            960: {
                slidesPerView: 4
            },
            768: {
                slidesPerView: 3
            },
            640: {
                slidesPerView: 1
            }

        }
    });

    var projectSlider = new Swiper('.swiperProjects', {
        spaceBetween: 30,
        slidesPerView: 4,
        loop: true,
        autoplay: {
            delay: 5000,
        },
        // setWrapperSize: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            960: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 2
            },
            640: {
                slidesPerView: 1
            }


        }
    });

});