<?php
remove_action( 'genesis_before_loop', 'genesis_do_posts_page_heading' );
add_action('genesis_before_loop', 'wst_display_featured_posts');
function wst_display_featured_posts(){
    $context   = Timber::get_context();
    $templates = array( 'featuredPosts.twig' );
    $context['featureds'] = Timber:: get_posts (array('post_type'=> 'post',
    'posts_per_page'=> 2
));
    Timber::render( $templates, $context );
}

add_action( 'genesis_loop', 'wst_custom_loop' );
remove_action( 'genesis_loop', 'genesis_do_loop' );

function wst_custom_loop() {
	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => 8,
		// 'offset'         => '2',
		'paged'          => get_query_var( 'paged' ),
	);
	global $wp_query;
	$wp_query = new WP_Query( $args );
	if ( have_posts() ) :
		echo '<h2>Recent Posts</h2>';
		echo '<div class="postsContainer">';
		while ( have_posts() ) :
			the_post();

			include 'includes/post.php';

		endwhile;
		echo '</div>';
		do_action( 'genesis_after_endwhile' );
	endif;
	wp_reset_query();

}



genesis();
