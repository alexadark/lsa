<?php

add_action( 'genesis_entry_content', 'wst_display_front_page' );

function wst_display_front_page() {
	$context   = Timber::get_context();
	$templates = array( 'front-page.twig' );
	Timber::render( $templates, $context );
}




genesis();
