<?php

remove_action( 'genesis_comment_form', 'genesis_do_comment_form' );
//* Remove the entry meta in the entry footer (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//* Force content-sidebar layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );

add_action( 'genesis_entry_header', 'wst_display_featured_image', 13 );
function wst_display_featured_image() {
	$args = array(
		'size' => 'large',
	);
	genesis_image( $args );
}

//* Customize the post info function
add_filter( 'genesis_post_info', 'sp_post_info_filter' );
function sp_post_info_filter( $post_info ) {
	if ( ! is_page() ) {
		$post_info = '[post_date] | [post_author_posts_link] [post_edit] ';
		return $post_info;
	}}


genesis();
